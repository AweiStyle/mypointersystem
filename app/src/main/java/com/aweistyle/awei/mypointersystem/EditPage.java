package com.aweistyle.awei.mypointersystem;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class EditPage extends AppCompatActivity {

    String[] data = {"976463462", "時鐘", "WIN", "53", "34", "-19"};
    String url;
    String DBName;
    String user;
    String password;
    String productBarcode1;
    String productName1;
    String productSpecification1;
    String qtyNow;
    String productUnit;
    String temp;
    String storeID_choose;

    String companyName;
    String version;

    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_page);

        DBName = getSharedPreferences("Log", MODE_PRIVATE).getString("DBName", "");
        user = getSharedPreferences("Log", MODE_PRIVATE).getString("user", "");
        password = getSharedPreferences("Log", MODE_PRIVATE).getString("password", "");
        url = getSharedPreferences("Log", MODE_PRIVATE).getString("url", "");

        Log.e("tag", "EditPage頁面成功取得DBName = " + DBName);
        Log.e("tag", "EditPage頁面成功取得user = " + user);
        Log.e("tag", "EditPage頁面成功取得password = " + password);
        Log.e("tag", "EditPage頁面成功取得url = " + url);

        final EditText editQuantity = findViewById(R.id.editQuantity);
        final TextView productBarcode = findViewById(R.id.productBarcode);
        final TextView productName = findViewById(R.id.productName);
        final TextView productSpecification = findViewById(R.id.productSpecification);
        final TextView qtyStock = findViewById(R.id.qtyStock);
         final TextView showUnit = findViewById(R.id.showUnit);
        Button save = findViewById(R.id.save);
        Button cancel = findViewById(R.id.cancel);

        companyName = getSharedPreferences("company_version", MODE_PRIVATE).getString("companyName", "");
        version = getSharedPreferences("company_version", MODE_PRIVATE).getString("version", "");

        TextView versionName = findViewById(R.id.version);
        versionName.setText( " " + companyName +  version);


        storeID_choose = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeID", "");

        editQuantity.setFilters(new InputFilter[]{lendFilter});

        Intent intent = getIntent();
        temp = intent.getStringExtra("temp");  //C000001

        Log.e("Allen", temp);
        productBarcode.setText(temp);

        Runnable runnable = new Runnable() {

            @Override
            public void run() {

                try {
                    //TextView textViewToChange = (TextView) findViewById(R.id.textViewCol5);
                    //textViewToChange.setText("Hello");
                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    Statement statement = con.createStatement();

                    String query = "SELECT A.partno, partna, spec, unit, QTY, A.STORNO from (SELECT PART.partno,partna,spec,unit, STORNA.STORNO FROM PART, STORNA) A LEFT JOIN StoreAmount B ON A.partno=B.PARTNO AND A.STORNO=B.STORNO WHERE A.partno = '" + temp + "' AND A.STORNO = '" + storeID_choose + "'" ;
                    Log.e("Allen", query);

                    ResultSet product = statement.executeQuery(query);

                    DecimalFormat df = new DecimalFormat("##0.0");

                    if (product.next()) {

                        productBarcode1 = product.getString(1);
                        productName1 = product.getString(2);
                        productSpecification1 = product.getString(3);
                        qtyNow = product.getString(5);
                        productUnit = product.getString(4);

                        Double.valueOf(qtyNow);
                        final String qtyNow1 = df.format(Double.valueOf(qtyNow));

                        Log.e("Allen", productBarcode1 + "/" + productName1 + "/" + productSpecification1);

//                        productBarcode.setText(product.getString(1));

                        Runnable r = new Runnable() {
                            @Override
                            public void run() {

                                productName.setText(productName1);
                                productSpecification.setText(productSpecification1);
                                qtyStock.setText(String.valueOf(Float.parseFloat(qtyNow1 == null ? "0" : qtyNow)));
//                                qtyStock.setText(String.valueOf((int)Float.parseFloat(qtyNow == null ? "0" : qtyNow)));
                                showUnit.setText(productUnit);

                            }
                        };

                        runOnUiThread(r);

                    } else {


                    }


                    product.close();
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        };

        new Thread(runnable).start();


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = 0;
                boolean b = false;

                DecimalFormat df = new DecimalFormat("##0.0");

                Double differenceQuantity = Double.valueOf(editQuantity.getText().toString()) - Double.valueOf(qtyStock.getText().toString());

                String editQuantity1 = df.format(Double.valueOf(editQuantity.getText().toString()));

                String differenceQuantity2 = df.format(differenceQuantity);

                ArrayList<String> product = new ArrayList<>();
                product.add(temp);
                product.add(productName.getText().toString());
                product.add(productSpecification.getText().toString());
                product.add(qtyStock.getText().toString());
                product.add(editQuantity1);
                product.add(String.valueOf(differenceQuantity2));
                product.add(productUnit);
                product.add("vno");  //不顯示
                product.add("SN");   //不顯示

                Log.e("arratList.size =",String.valueOf(product.size()));



                SharedPreferences sharedPreferences = getSharedPreferences("product", MODE_PRIVATE);
                String a = sharedPreferences.getString("pointQuantity", "[]");

                Log.e("Allen", "read:" + a);

                ArrayList<ArrayList<String>> arrayList = new Gson().fromJson(a, ArrayList.class);


                for(i = 0 ;  i < arrayList.size() ; i++){    //資料重複（修改）

                    if (arrayList.get(i).get(0).equals(productBarcode1)) {

                        b = true;
                        arrayList.get(i).set(4, editQuantity.getText().toString());
                        arrayList.get(i).set(5,String.valueOf(differenceQuantity));

                    }

                }

                if(b == false) arrayList.add(product);  //資料未重複（新增）

                gson = new Gson();
                String json = gson.toJson(arrayList);
                sharedPreferences.edit().putString("pointQuantity", json).putString("productUnit",productUnit).commit();


                Log.e("Allen", "save:" + json);

                finish();

                Log.e("Allen", "finish");

//                new Thread(runnable1).start();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(EditPage.this);
                builder.setTitle("資料尚未儲存");
                builder.setMessage("確定不儲存並離開此頁面嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        EditPage.this.finish();

                    }
                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }

                });

                builder.show();

            }
        });

//        editQuantity.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                String text = s.toString();
//                int len = s.toString().length();
//                if (len > 1 && text.startsWith("0")) {
//                    s.replace(0,1,"");
//                }
//            }
//        });

    }

    private InputFilter lendFilter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            // 删除等特殊字符，直接返回
            if ("".equals(source.toString())) {
                return null;
            }
            String dValue = dest.toString();
            String[] splitArray = dValue.split("\\.");
            if (splitArray.length > 1) {
                String dotValue = splitArray[1];
                int diff = dotValue.length() + 1 - 1;//4表示输入框的小数位数
                if (diff > 0) {
                    return source.subSequence(start, end - diff);
                }
            }
            return null;
        }
    };

    @Override
    public void onBackPressed() {

    }

}
