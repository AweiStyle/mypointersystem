package com.aweistyle.awei.mypointersystem;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StartPointer extends AppCompatActivity {


    String url;
    String DBName;
    String user;
    String password;

    Gson gson;
    String[][] items;
    ListView list_startPointer;
    ArrayList<String> arrayList;
    String json;
    //    Spinner spn_pointList;
    String[] autoComplete = {"ab", "abc", "ac", "bc"};
    Adapter_wareHouse adapter_wareHouse;
    String inputBarcode;
    String productNO;
    Runnable runnable1;
    String vno;
    String vno_final;  //最終單號（盤點單）
    String staffID;
    String staffName;
    String storeID_choose;
    String storeName;
    Long latestSN;
    String productUnit;
    ViewAdapter adapter;
    ProgressBar progressBar_upload;

    String companyName;
    String version;


    @Override
    protected void onResume() {  //每次回到本頁面後執行
        super.onResume();

        Log.e("Allen", "pageResume");

        if (haveInternet() == false){

            Log.e("testConnection","noInternet");

            Toast toast = Toast.makeText(StartPointer.this, "請確認網路是否連接", Toast.LENGTH_LONG);
            toast.show();

        }

        DBName = getSharedPreferences("Log", MODE_PRIVATE).getString("DBName", "");
        user = getSharedPreferences("Log", MODE_PRIVATE).getString("user", "");
        password = getSharedPreferences("Log", MODE_PRIVATE).getString("password", "");
        url = getSharedPreferences("Log", MODE_PRIVATE).getString("url", "");

        Log.e("tag", "startPointer頁面成功取得DBName = " + DBName);
        Log.e("tag", "startPointer頁面成功取得user = " + user);
        Log.e("tag", "startPointer頁面成功取得password = " + password);
        Log.e("tag", "startPointer頁面成功取得url = " + url);

        staffID = getSharedPreferences("store_login", MODE_PRIVATE).getString("staffID", "");
        staffName = getSharedPreferences("store_login", MODE_PRIVATE).getString("staffName", "");
        storeID_choose = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeID", "");
        storeName = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeName", "");
        productUnit = getSharedPreferences("pointQuantity", MODE_PRIVATE).getString("productUnit", "");


        Log.e("tag", "startPointer頁面成功取得staffID = " + staffID);
        Log.e("tag", "startPointer頁面成功取得staffName = " + staffName);
        Log.e("tag", "startPointer頁面成功取得storeID_choose = " + storeID_choose);
        Log.e("tag", "storeName = " + storeName);

        companyName = getSharedPreferences("company_version", MODE_PRIVATE).getString("companyName", "");
        version = getSharedPreferences("company_version", MODE_PRIVATE).getString("version", "");

        TextView versionName = findViewById(R.id.version);
        versionName.setText( " " + companyName + version);

        String json = getSharedPreferences("product", MODE_PRIVATE).getString("pointQuantity", "[]");
        //讀取SharedPreference

        Log.e("Allen", "read:" + json);


        try {

            Log.e("Allen", "try");

            gson = new Gson();
            items = gson.fromJson(json, String[][].class);  //利用Gson把字串轉為二維陣列

            adapter = new ViewAdapter(items);   //建立ViewAdapter並帶入二維陣列
            list_startPointer.setAdapter(adapter);          //把ViewAdapter設定給ListView

        } catch (Exception e) {

            Log.e("Allen", "catch");

            items = new String[][]{};

            adapter = new ViewAdapter(items);
            list_startPointer.setAdapter(adapter);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {

                Toast.makeText(this, "退出掃描", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(this, result.getBarcodeImagePath(), Toast.LENGTH_SHORT).show();
                String scannerBarcode = result.getContents();
                checkData(scannerBarcode);

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_pointer);

        Button scannerCode = findViewById(R.id.scannerCode);
        final Button userInput = findViewById(R.id.userInput);
        Button uploadDB = findViewById(R.id.uploadDB);
        list_startPointer = findViewById(R.id.list_startPointer);
        final Activity activity = this;

        SharedPreferences sharedPreferences = getSharedPreferences("product", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();


//        AutoCompleteTextView autoCompleteTextView = findViewById(R.id.autoCompleteTextView);
//
//        final ArrayAdapter<String> adapter
//                = new ArrayAdapter<String>(this,
//                android.R.layout.simple_dropdown_item_1line,
//                autoComplete); //宣告 Adapter
//        autoCompleteTextView.setAdapter(adapter); //設定 Adapter 給 mAutoCompleteTextView
//        autoCompleteTextView.setThreshold(1); //設定輸入幾個字後開始比對
////        autoCompleteTextView.setDropDownBackgroundResource(R.drawable.yellow);//設定背景圖片
////        autoCompleteTextView.setDropDownBackgroundDrawable(getResource().getDrawable(R.drawable.yellow));//設定背景圖片
//        autoCompleteTextView.setDropDownWidth(400); //設定寬度
//        autoCompleteTextView.setDropDownHeight(400); //設定高度
////        autoCompleteTextView.setOnItemClickListener(AutoCompleteTextViewOnItemClickListener);


        scannerCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {          //開啟相機

                Log.e("Allen", "scan1");

                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();


            }
        });

//        userInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                                               @Override
//                                               public void onFocusChange(View v, boolean hasFocus) {
//
//                                                   if(hasFocus){
//                                                       userInput.setSingleLine(true);
//                                                       userInput.setMaxLines(1);
//                                                       userInput.setLines(1);
//
//                                               }
//                                           }});

                userInput.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.e("Allen", "userInput");


                        final EditText edit = new EditText(StartPointer.this);

                        AlertDialog.Builder builder = new AlertDialog.Builder(StartPointer.this);
                        builder.setTitle("請輸入國際條碼");    //设置对话框标题
                        builder.setIcon(android.R.drawable.btn_star);

                        edit.setSingleLine();
                        edit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                        builder.setView(edit);
                        builder.setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {



                                try {

                                    inputBarcode = edit.getText().toString();
                                    checkData(inputBarcode);

                                } catch (Exception e) {

                                    Toast toast2 = Toast.makeText(StartPointer.this, "請確實輸入產品編號", Toast.LENGTH_LONG);
                                    toast2.show();

                                }


                            }
                        });
                        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }


                        });

                        builder.show();


                    }

                });

        uploadDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (haveInternet() == false){

                    Log.e("testConnection","noInternet");

                    Toast toast = Toast.makeText(StartPointer.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                    toast.show();

                    return;

                }

                progressBar_upload = findViewById(R.id.progressBar_upload);

                AlertDialog.Builder builder = new AlertDialog.Builder(StartPointer.this);
                builder.setTitle("上傳盤點單");
                builder.setMessage("確定上傳資料嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(items.length != 0) {

                            progressBar_upload.setVisibility(View.VISIBLE);

                            new Thread(runnable_VNO).start();

                            SharedPreferences sharedPreferences = getSharedPreferences("product", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear().commit();



                        }else {

                            Toast toast = Toast.makeText(StartPointer.this, "請新增明細後再上傳", Toast.LENGTH_LONG);
                            toast.show();

                        }

                    }
                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }

                });

                builder.show();


            }
        });

        list_startPointer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //ListView的點擊效果

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


                arrayList = new ArrayList(Arrays.asList(items));  //把array轉換成arrayList
                //arrayList才能做陣列刪減

                final AlertDialog.Builder builder = new AlertDialog.Builder(StartPointer.this);
                builder.setTitle("刪除資料");
                builder.setMessage("確定要刪除這筆資料嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        arrayList.remove(position);     //刪除點擊的列表項目


                        Log.e("Allen", "delete");

                        items = arrayList.toArray(new String[0][0]);
                        String a = gson.toJson(items);

                        SharedPreferences sharedPreferences = getSharedPreferences("product", MODE_PRIVATE);  //7寫入資料
                        sharedPreferences.edit().putString("pointQuantity", a).commit();

                        Log.e("Allen", "after del to saveBack:" + a);

                        ViewAdapter adapter = new ViewAdapter(items);  //77~78行 呼叫adapter來顯示內容
                        list_startPointer.setAdapter(adapter);
                    }
                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        Log.e("Allen", "cancel");

                    }

                });

                builder.show();

            }
        });


        Runnable runnable = new Runnable() {      //DB帶入spinner

            @Override
            public void run() {

                try {

                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    Statement statement = con.createStatement();
                    ResultSet store = statement.executeQuery("SELECT STORNA,STORNAME FROM dbo.STORNA;");

                    ArrayList<String[]> arrayList = new ArrayList<>();
                    while (store.next()) {

                        String storeA = store.getString(1);
                        String storeB = store.getString(2);
                        //Log.e("Allen", storeA + "" + storeB);

                        arrayList.add(new String[]{storeA, storeB});
                    }

                    store.close();
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        };

        new Thread(runnable).start();


    }

    public void checkData(final String code) {


        runnable1 = new Runnable() {

            @Override
            public void run() {

                try {

                    if (haveInternet() == false){

                        Log.e("testConnection","noInternet");

                        throw new ConnectException();

                    }

                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    Statement statement = con.createStatement();

                    String query = "SELECT PART.partno , PART.BarcodeNO , PART.ISDel FROM PART WHERE PART.BarcodeNO = '" + code + "' AND PART.ISDel = 'false'";
                    Log.e("Allen", query);

                    ResultSet product = statement.executeQuery(query);

                    if (product.next()) {

                        productNO = product.getString(1);  //partno

                        Intent intent = new Intent(StartPointer.this, EditPage.class).putExtra("temp", productNO);
                        startActivity(intent);

                    } else {

                        Looper.prepare();
                        Toast toast = Toast.makeText(StartPointer.this, "查無此商品,新增商品請洽後端人員", Toast.LENGTH_LONG);
                        toast.show();
                        Looper.loop();

                    }


                    product.close();
                    statement.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }catch (ConnectException e){

                    Looper.prepare();
                    Toast toast = Toast.makeText(StartPointer.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                }


            }
        };
        new Thread(runnable1).start();
    }

    Runnable runnable_VNO = new Runnable() {

        @Override
        public void run() {

            try {


                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                Date curDate = new Date(System.currentTimeMillis());
                String date = formatter.format(curDate);
                vno = "MAKA" + date;
                Log.e("tag", vno);

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                Statement statement = con.createStatement();

                String vnoResult = "Select MAX(VNO) from STKADJH where VNO like '" + vno + "%'";
                Log.e("runnable_VNO query:", vnoResult);


                ResultSet resultSet = statement.executeQuery(vnoResult);

                while (resultSet.next()) {

                    try {

                        String vno_temp = resultSet.getString(1);
                        int vno_temp1 = Integer.parseInt(vno_temp.substring(12)) + 1;
                        Log.e("vno_temp1 =", vno_temp1 + "");
                        vno_final = vno + String.format("%04d", vno_temp1);
                        Log.e("runnable_VNO if:", "vno_final=" + vno_final);

                    } catch (Exception e) {

                        vno_final = vno + "0001";   //單號

                        Log.e("runnable_VNO else:", "vno_final=" + vno_final);

                    }

                    Log.e("runnable_VNO", "finish");

                }

                resultSet.close();
                statement.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            //Insert STKADJH
            //( GID ,        VNO,      VDATE,         STORNO,        STORNA ,    StaffID ,  StaffNA , Confirmed )
            //( NEWID() , vno_final , GetDate() , storeID_choose ,  storeName ,  staffID , staffName ,NULL )

            try {

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                Statement statement = con.createStatement();

                String vnoResult = "INSERT INTO STKADJH (GID ,VNO, VDATE, STORNO, STORNA , StaffID , StaffNA , Confirmed , ISDel) VALUES( NEWID() , '" + vno_final + "', GetDate() , '" + storeID_choose + "', '" + storeName + "','" + staffID + "','" + staffName + "', NULL , 'false');";
                Log.e("runnable_VNO query:", vnoResult);


                statement.execute(vnoResult);

                statement.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }



            //Insert STKADJS

            try {

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");


                String vnoResult = "INSERT INTO STKADJS (HEADSN ,HGID, VNO, SNO , PARTNO, PARTNA , SPEC , UNIT , STKQTY , NEWQTY , DIFFQTY ,STORNO) VALUES";

                for (int i = 0; i < items.length; i++) {
                    String[] item = items[i];

                    int sno = i + 1;

                    String subquery = "((SELECT SN FROM STKADJH WHERE VNO = '" + vno_final + "'), (SELECT GID FROM STKADJH WHERE VNO = '" + vno_final + "'), '" + vno_final + "','" + sno  + "', '" + item[0] + "', '" + item[1] + "', '" + item[2] + "', '" + item[6] + "'," + item[3] + ", " + item[4] + ", " + (Integer.parseInt(item[4]) - Integer.parseInt(item[3])) + ", '" + storeID_choose + "'),";
                    vnoResult += subquery;



                }
                vnoResult = vnoResult.substring(0, vnoResult.length() - 1);

                Log.e("runnable_VNO query:", vnoResult);

                Statement statement = con.prepareStatement(vnoResult, Statement.RETURN_GENERATED_KEYS);
                ((PreparedStatement) statement).executeUpdate();


                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    latestSN = generatedKeys.getLong(1);
                }


                statement.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }



            //Insert storeIn

            try {

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");


                for (int i = 0; i < items.length; i++) {

                    Statement statement = con.createStatement();

                    Long SN = latestSN - (items.length - i - 1);


                    String insertResult = "Insert  Into StoreIN (VNO,SourceGID,SourceSN,STORNO,PARTNO,QTY,StockQTY,BranchStockQTY) SELECT '" + vno_final + "',STKADJS.GID," +
                            "STKADJS.SN," +
                            "STKADJS.STORNO,STKADJS.PARTNO, STKADJS.DIFFQTY, COALESCE(PART.QTY,0) + STKADJS.DIFFQTY as StockQTY,COALESCE(StoreAmount.QTY,0) + STKADJS.DIFFQTY  AS BranchQTY FROM   STKADJS LEFT JOIN PART ON STKADJS.PARTNO = PART.PARTNO LEFT JOIN StoreAmount ON STKADJS.PARTNO = StoreAmount.PARTNO AND STKADJS.STORNO = StoreAmount.STORNO where STKADJS.VNO='" + vno_final + "' and STKADJS.SN = '" + SN + "'";

                    Log.e("insertResult", insertResult);

                    statement.execute(insertResult);

                    statement.close();
                }


            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            finish();

        }

    };

    private boolean haveInternet()
    {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
        {
            result = false;
        }
        else
        {
            if (!info.isAvailable())
            {
                result =false;
            }
            else
            {
                result = true;
            }
        }

        return result;
    }


}



