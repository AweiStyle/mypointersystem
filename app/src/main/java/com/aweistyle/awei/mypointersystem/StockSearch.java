package com.aweistyle.awei.mypointersystem;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class StockSearch extends AppCompatActivity {
    Spinner spinner1;
    String url;
    String DBName;
    String user;
    String password;
    String storeID_choose;
    int storeID_chooseInt;
    Adapter_wareHouse adapter_wareHouse;
    ViewAdapter_stockSearch viewAdapter_stockSearch;

    ArrayList<String> storeID = new ArrayList<>();
    ArrayList<String> autoComplete = new ArrayList<>();
    AutoCompleteTextView autoCompleteTextView1;
    String barcode;
    String productName;
    String spec;
    String unit;
    String stockQTY;
    String totalQTY;
    ListView stockSearchList;
    String[][] items;
    String[][] empty = {};
    String scanResult;
    Runnable getData_scan;

    String companyName;
    String version;
    Thread thread;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "退出掃描", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, result.getBarcodeImagePath(), Toast.LENGTH_SHORT).show();
                scanResult = result.getContents();
                checkData(scanResult);

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_search);


        if (haveInternet() == false) {

            Log.e("testConnection", "noInternet");

            Toast toast = Toast.makeText(StockSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
            toast.show();

        }

        spinner1 = findViewById(R.id.spinner1);
        autoCompleteTextView1 = findViewById(R.id.autoCompleteTextView1);
        final Activity activity = this;

        DBName = getSharedPreferences("Log", MODE_PRIVATE).getString("DBName", "");
        user = getSharedPreferences("Log", MODE_PRIVATE).getString("user", "");
        password = getSharedPreferences("Log", MODE_PRIVATE).getString("password", "");
        url = getSharedPreferences("Log", MODE_PRIVATE).getString("url", "");

        companyName = getSharedPreferences("company_version", MODE_PRIVATE).getString("companyName", "");
        version = getSharedPreferences("company_version", MODE_PRIVATE).getString("version", "");

        TextView versionName = findViewById(R.id.version);
        versionName.setText(" " + companyName + version);

        storeID_choose = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeID", "");
        String storeID_chooseTemp = getSharedPreferences("store_login", MODE_PRIVATE).getString("storeID_int", "");
        storeID_chooseInt = Integer.parseInt(storeID_chooseTemp);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                new Thread(getData).start();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        autoCompleteTextView1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (thread != null) {


                    Log.e("thread.interrupt():", "thread.interrupt()");

                    thread.interrupt();
                }

                thread = new Thread(getData);
                thread.start();

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (haveInternet() == false) {

                    Log.e("testConnection", "noInternet");

                    Toast toast = Toast.makeText(StockSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                    toast.show();

                }

            }
        });

        Button bt_1 = findViewById(R.id.bt1);

        bt_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Allen", "scan1");

                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();


            }
        });

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                try {

                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"

                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    Statement statement = con.createStatement();
                    ResultSet store = statement.executeQuery("SELECT STORNA,STORNAME,STORNO FROM dbo.STORNA;");

                    ArrayList<String[]> arrayList = new ArrayList<>();
                    while (store.next()) {

                        String storeA = store.getString(1);
                        String storeB = store.getString(2);
                        //Log.e("Allen", storeA + "" + storeB);

                        arrayList.add(new String[]{storeA, storeB});

                        storeID.add(store.getString(3));
                    }

                    spinner1 = findViewById(R.id.spinner1);

                    String[][] warehouse = arrayList.toArray(new String[][]{});
                    adapter_wareHouse = new Adapter_wareHouse(warehouse);

                    Runnable r = new Runnable() {
                        @Override
                        public void run() {

                            spinner1.setAdapter(adapter_wareHouse);
                            spinner1.setSelection(storeID_chooseInt);

                        }
                    };

                    runOnUiThread(r);

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        };

        new Thread(runnable).start();

    }

    Runnable r4 = new Runnable() {
        @Override
        public void run() {

            Log.e("Runnable =>", "r4");

            viewAdapter_stockSearch = new ViewAdapter_stockSearch(empty);
            stockSearchList.setAdapter(viewAdapter_stockSearch);

        }
    };

    public void checkData(final String code) {

        getData_scan = new Runnable() {
            @Override
            public void run() {

                try {

                    if (haveInternet() == false) {

                        Log.e("testConnection", "noInternet");

                        throw new ConnectException();

                    }

                    Class.forName("net.sourceforge.jtds.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"
                            + "databaseName=" + DBName + ";user=" + user
                            + ";password=" + password + ";");

                    String query_scan = "WITH V(PARTNO, QTY) AS (SELECT PARTNO, QTY FROM StoreAmount WHERE (STORNO = '" + storeID.get(spinner1.getSelectedItemPosition()) + "'))" +
                            "SELECT COALESCE (V_1.QTY, 0) AS QTY, PART.PARTNA, PART.PARTNO, PART.QTY AS PARTQTY, PART.SN, PART.SPEC, PART.UNIT FROM PART LEFT OUTER JOIN V AS V_1 ON PART.PARTNO = V_1.PARTNO where PART.BarcodeNO = '" + code + "'";


                    Log.e("Start_search_checkData:", query_scan);


                    Statement statement = con.createStatement();
                    ResultSet resultSet = statement.executeQuery(query_scan);

                    DecimalFormat df = new DecimalFormat("##0.0");

                    final ArrayList<String[]> arrayList = new ArrayList<>();
                    while (resultSet.next()) {

                        Log.e("checkData:", "if");

                        barcode = resultSet.getString(3);
                        productName = resultSet.getString(2);
                        spec = resultSet.getString(6);
                        unit = resultSet.getString(7);
                        stockQTY = resultSet.getString(1);
                        totalQTY = resultSet.getString(4);

                        Double.valueOf(stockQTY);
                        String stockQTY1 = df.format(Double.valueOf(stockQTY));

                        Double.valueOf(totalQTY);
                        String totalQTY1 = df.format(Double.valueOf(totalQTY));

                        arrayList.add(new String[]{barcode, productName, spec, unit, stockQTY1, storeID.get(spinner1.getSelectedItemPosition()), totalQTY1});

                    }

                    if (arrayList.size() == 0) throw new SQLException();


                    stockSearchList = findViewById(R.id.searchList);

                    items = arrayList.toArray(new String[][]{});
                    viewAdapter_stockSearch = new ViewAdapter_stockSearch(items);

                } catch (ClassNotFoundException e) {

                    Log.e("checkData:", "catch1");

                    e.printStackTrace();
                } catch (SQLException e) {

                    Log.e("checkData:", "catch2");

                    runOnUiThread(r4);

                    Looper.prepare();
                    Toast toast = Toast.makeText(StockSearch.this, "尚未盤點過此商品", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                    e.printStackTrace();
                } catch (ConnectException e) {

                    Looper.prepare();
                    Toast toast = Toast.makeText(StockSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                }


                Runnable r2 = new Runnable() {
                    @Override
                    public void run() {

                        Log.e("tag", "setView");

                        stockSearchList.setAdapter(viewAdapter_stockSearch);

                    }
                };
                runOnUiThread(r2);

            }
        };

        new Thread(getData_scan).start();

    }

    Runnable getData = new Runnable() {
        @Override
        public void run() {

            try {

                Thread.sleep(500);

                if (haveInternet() == false) {

                    Log.e("testConnection", "noInternet");

                    throw new ConnectException();

                }

                if (storeID.size() == 0) {

                    Looper.prepare();
                    Toast toast = Toast.makeText(StockSearch.this, "如已接上網路.請關閉頁面後重試", Toast.LENGTH_LONG);
                    toast.show();
                    Looper.loop();

                    return;

                }

                Class.forName("net.sourceforge.jtds.jdbc.Driver");
                Connection con = DriverManager.getConnection("jdbc:jtds:sqlserver://" + url + ";"
                        + "databaseName=" + DBName + ";user=" + user
                        + ";password=" + password + ";");

                String query = "WITH V(PARTNO, QTY) AS (SELECT PARTNO, QTY FROM StoreAmount WHERE (STORNO = '" + storeID.get(spinner1.getSelectedItemPosition()) + "'))" +
                        "SELECT COALESCE (V_1.QTY, 0) AS QTY, PART.PARTNA, PART.PARTNO, PART.QTY AS PARTQTY, PART.SN, PART.SPEC, PART.UNIT FROM PART LEFT OUTER JOIN V AS V_1 ON PART.PARTNO = V_1.PARTNO where PART.PARTNA LIKE '%" + autoCompleteTextView1.getText() + "%'";

                Log.e("query=", query);

                Statement statement = con.createStatement();
                ResultSet resultSet = statement.executeQuery(query);

                autoComplete.clear();
                DecimalFormat df = new DecimalFormat("##0.0");
                final ArrayList<String[]> arrayList = new ArrayList<>();

                while (resultSet.next()) {

                    barcode = resultSet.getString(3);
                    Log.e("barcode = ", barcode);
                    productName = resultSet.getString(2);
                    spec = resultSet.getString(6);
                    unit = resultSet.getString(7);
                    stockQTY = resultSet.getString(1);
                    totalQTY = resultSet.getString(4);

                    String name = resultSet.getString(2);

                    Double.valueOf(stockQTY);
                    String stockQTY1 = df.format(Double.valueOf(stockQTY));

                    Double.valueOf(totalQTY);
                    String totalQTY1 = df.format(Double.valueOf(totalQTY));

                    arrayList.add(new String[]{barcode, productName, spec, unit, stockQTY1, storeID.get(spinner1.getSelectedItemPosition()), totalQTY1});

                    if (!autoComplete.contains(name)) autoComplete.add(name);

                }


                stockSearchList = findViewById(R.id.searchList);

                items = arrayList.toArray(new String[][]{});
                viewAdapter_stockSearch = new ViewAdapter_stockSearch(items);

                Gson gson = new Gson();
                String a = gson.toJson(items);

                Log.e("items = ", a);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Log.e("tag", "autoComplete");

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                                StockSearch.this,
                                android.R.layout.simple_dropdown_item_1line,
                                autoComplete); //宣告 Adapter
                        autoCompleteTextView1.setAdapter(adapter); //設定 Adapter 給 mAutoCompleteTextView
                        autoCompleteTextView1.setThreshold(1); //設定輸入幾個字後開始比對
//        autoCompleteTextView.setDropDownBackgroundResource(R.drawable.yellow);//設定背景圖片
//        autoCompleteTextView.setDropDownBackgroundDrawable(getResource().getDrawable(R.drawable.yellow));//設定背景圖片
                        autoCompleteTextView1.setDropDownWidth(400); //設定寬度
                        autoCompleteTextView1.setDropDownHeight(400); //設定高度
//        autoCompleteTextView.setOnItemClickListener(AutoCompleteTextViewOnItemClickListener);
                    }
                });


            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ConnectException e) {


                Looper.prepare();
                Toast toast = Toast.makeText(StockSearch.this, "請確認網路是否連接", Toast.LENGTH_LONG);
                toast.show();
                Looper.loop();

            } catch (InterruptedException e) {
                e.printStackTrace();

                return;
            }

            Runnable r1 = new Runnable() {
                @Override
                public void run() {

                    Log.e("setView", "setView");

                    stockSearchList.setAdapter(viewAdapter_stockSearch);

                }
            };


            runOnUiThread(r1);
        }
    };

    private boolean haveInternet() {
        boolean result = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            result = false;
        } else {
            if (!info.isAvailable()) {
                result = false;
            } else {
                result = true;
            }
        }

        return result;
    }


}
